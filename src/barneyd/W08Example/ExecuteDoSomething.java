package W08Example;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteDoSomething {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        DoSomething ds1 = new DoSomething("Bob", 25, 1000);
        DoSomething ds2 = new DoSomething("Sally", 10, 500);
        DoSomething ds3 = new DoSomething("Billy", 5, 250);
        DoSomething ds4 = new DoSomething("Anna", 2, 100);
        DoSomething ds5 = new DoSomething("Pat", 1, 50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}