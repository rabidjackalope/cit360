package JSONandHTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPConnect {

     public static void main(String[] args) throws IOException {

        URL url = new URL("https://fonts.google.com/");

        HttpURLConnection openConnection = (HttpURLConnection)url.openConnection();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));

        String httpCode;
        try {
            while((httpCode = bufferedReader.readLine()) != null) {
                if(!httpCode.isEmpty()) {
                    System.out.println(httpCode);
                } else {
                    System.out.println(" ");
                }
            }

            System.out.println();
            System.out.println("HTTP Headers: ");
            for(int i=1;i<=8;i++){
                System.out.println(openConnection.getHeaderFieldKey(i)+" = "+openConnection.getHeaderField(i));
            }
            bufferedReader.close();

        } catch (IOException e) {
            System.err.println(e.toString());
        }
    }
}