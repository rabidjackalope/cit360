package JSONandHTTP;

public class Inventory2 {

    private int year;
    private String make;
    private String model;
    private String color;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Year: " + year +"\n"+ "Make: " + make +"\n"+ "Model: " + model +"\n"+ "Color: " + color;
    }
}
