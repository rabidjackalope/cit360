package JSONandHTTP;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;


public class JSON {

    public static String inventoryToJSON (Inventory2 inventory) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(inventory);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }


        return s;
    }

    public static Inventory2 JSONToInventory2(String s) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Inventory2 invent = null;

        try {
            invent = mapper.readValue(s, Inventory2.class);
        }catch (JsonProcessingException e){
            System.err.println(e.toString());
        }

        return invent;
    }

    public static void main(String[] args) throws Exception {

        Inventory2 inv = new Inventory2();
        inv.setYear(1967);
        inv.setMake("Chevy ");
        inv.setModel("Impala");
        inv.setColor("Black");

        String json = JSON.inventoryToJSON(inv);
        System.out.println(json);

        Inventory2 inv2 = JSON.JSONToInventory2(json);
        System.out.println(inv2);

    }

}
