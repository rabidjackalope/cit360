package com.example.CarShowEntryForm;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "Servlet", value = "/Servlet")
public class Servlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String year = request.getParameter("year");
        String make = request.getParameter("make");
        String model = request.getParameter("model");
        String color = request.getParameter("color");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");

        out.println("<h1>Vehicle Entry Recorded</h1>");
        out.println("<p>Your entry has been recorded. Remember to arrive at 7:30am on " +
                "Saturday, Octember 32nd to set up your area.<br/>" +
                "Please contact the <em>Hello World! Car Show</em> administrators at " +
                "(208) 555-0178 to correct any information.</p>");
        out.println("<p><u>Your contact information:</u><br/>" +
                fname + " " + lname + "<br/>" + phone + "<br/>" + email + "</p>");
        out.println("<p><u>Your vehicle information:</u><br/>" +
                year + " " + make + " " + model + " " + color + "</p>");
        out.println("</body></html>");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");

    }
}