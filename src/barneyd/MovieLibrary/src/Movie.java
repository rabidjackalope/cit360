public class Movie
{
    private String title;
    private String director;
    private String mpaaRating;
    private String format;

    public Movie(String movieTitle, String movieDirector, String movieMPAARating, String movieFormat)
    {
        if ((movieTitle.trim().length() == 0) || (movieTitle == null) ||
                (movieDirector.trim().length() == 0) || (movieDirector == null) ||
                (movieMPAARating.trim().length() == 0) || (movieMPAARating == null) ||
                (movieFormat.trim().length() == 0) || (movieFormat == null))
        {
            throw new IllegalStateException("Fields cannot be blank.");
        }
        title = movieTitle;
        director = movieDirector;
        mpaaRating = movieMPAARating;
        format = movieFormat;
    }

    public void displayMovieInformation()
    {
        System.out.print("Title: " + title + "\n");
        System.out.print("Director: " + director + "\n");
        System.out.print("MPAA Rating: " + mpaaRating + "\n");
        System.out.print("Format: " + format + "\n");
        System.out.print("\n");
    }

    public String getDirector()
    {
        return director;
    }

    public String getMPAARating()
    {
        return mpaaRating;
    }

    public String getFormat()
    {
        return format;
    }

    public String getTitle()
    {
        return title;
    }
}
