import java.util.ArrayList;
import java.util.Iterator;

public class Controller
{
    private Database movieDatabase;

    public Controller()
    {
        movieDatabase = new Database();
        movieDatabase.loadDatabaseFromFile("mymovies.txt");
    }

    public static void main(String[] args){
        Controller movieLibrary = new Controller();
        movieLibrary.runDriver();
    }

    public void runDriver()
    {
        boolean continueProgram = true;
        int optionChosen = 0;

        String menuTitle = "\nMovie Database\n";
        ArrayList<String> menuOptions = new ArrayList<String>();
        menuOptions.add("Search for a movie\n");
        menuOptions.add("Add movie\n");
        menuOptions.add("Delete movie\n");
        menuOptions.add("Exit\n");
        try
        {
            Menu driverMenu = new Menu(menuTitle,menuOptions);
            UserInput userInput = new UserInput();
            while (continueProgram)
            {
                userInput.clearScreen();
                driverMenu.displayMenu();

                optionChosen = userInput.readIntegerFromUser(4,
                        "Please select from options above (1-4): ");
                userInput.clearScreen();

                switch (optionChosen)
                {
                    case 1:
                        searchForMovie();
                        userInput.pressEnterToContinue();
                        break;
                    case 2:
                        Movie newMovie = readMovieInformationFromUser();
                        movieDatabase.addMovie(newMovie);
                        break;
                    case 3:
                        deleteMovie();
                        userInput.pressEnterToContinue();
                        break;
                    case 4:
                        continueProgram = false;
                        movieDatabase.saveMoviesToFile("mymovies.txt");
                        movieDatabase.clearAll();
                        break;
                    default:
                        System.out.print("\nError! That option does not exist!\n");
                        break;
                }
            }
        }
        catch (IllegalStateException e)
        {
            System.out.print("\nError creating menu!\n");
        }
    }

    private void searchForMovie()
    {
        try
        {
            String searchMenuTitle = "\nMovie Search\n";
            ArrayList<String> searchMenuOptions = new ArrayList<String>();
            searchMenuOptions.add("Search by title\n");
            searchMenuOptions.add("Search by director\n");
            searchMenuOptions.add("Search by MPAA Rating\n");
            Menu searchOptionMenu = new Menu(searchMenuTitle,searchMenuOptions);

            UserInput userInput = new UserInput();
            int searchOptionChosen = 0;
            while (searchOptionChosen == 0)
            {
                userInput.clearScreen();
                searchOptionMenu.displayMenu();
                searchOptionChosen = userInput.readIntegerFromUser(3,
                        "Please select from options above (1-3): ");
            }

            String searchKey = "";
            if (searchOptionChosen == 1)
            {
                searchKey = "title";
            }
            else if (searchOptionChosen == 2)
            {
                searchKey = "director";
            }
            else
            {
                searchKey = "mpaaRating";
            }

            String searchString = userInput.readStringFromUser("Enter "+searchKey+" to search for: ",
                    false);
            ArrayList<Movie> foundMovies = movieDatabase.searchForMovie(searchString,searchKey);
            if (foundMovies.size() == 0)
            {
                System.out.print("\nNo movies found.\n\n");
            }
            else
            {
                System.out.print("\n");
                Iterator<Movie> it = foundMovies.iterator();
                while (it.hasNext())
                {
                    Movie movie = it.next();
                    movie.displayMovieInformation();
                }
            }
        }
        catch (IllegalStateException e)
        {
            System.out.print("\nError creating menu!\n");
        }
    }

    private Movie readMovieInformationFromUser()
    {
        UserInput userInput = new UserInput();

        String messagePromptForUser = "\nPlease enter the movie title: ";
        boolean titleAlreadyExists = true;
        String title = "";
        while (titleAlreadyExists)
        {
            userInput.clearScreen();
            title = userInput.readStringFromUser(messagePromptForUser, false);
            ArrayList<Movie> moviesMatchingTitle = movieDatabase.searchForMovie(title,"title");
            if (moviesMatchingTitle.size() == 0)
            {
                titleAlreadyExists = false;
            }
            else
            {
                System.out.print("\nError: That title already exists.\n\n");
                userInput.pressEnterToContinue();
            }
        }

        messagePromptForUser = "\nPlease enter the director's name: ";
        String director = userInput.readStringFromUser(messagePromptForUser, false);

        messagePromptForUser = "\nPlease enter the MPAA Rating: ";
        String mpaaRating = userInput.readStringFromUser(messagePromptForUser, false);

        messagePromptForUser = "\nPlease enter the movie format. Separate each format by a semicolon." +
                "\n\t(i.e., DVD; VHS; Blu-Ray; LaserDisc): ";
        String format = userInput.readStringFromUser(messagePromptForUser, false);

        try
        {
            Movie newMovie = new Movie(title, director, mpaaRating, format);
            return newMovie;
        }
        catch (IllegalStateException e)
        {
            System.out.print(e.getMessage());
            return null;
        }
    }

    private void deleteMovie()
    {
        UserInput userInput = new UserInput();
        String movieTitleToDelete = userInput.readStringFromUser("Enter movie title to delete: ",
                false);

        ArrayList<Movie> moviesForDelete = movieDatabase.searchForMovie(movieTitleToDelete,"title");

        if (moviesForDelete.size() == 0)
        {
            System.out.print("\nNo movies found matching that title.\n\n");
        }
        else
        {
            System.out.print("\n");
            moviesForDelete.get(0).displayMovieInformation();

            try
            {
                String deleteMenuTitle = "\nAre you sure you want to delete: " +
                        moviesForDelete.get(0).getTitle()  + "?\n";
                ArrayList<String> deleteMenuOptions = new ArrayList<String>();
                deleteMenuOptions.add("Yes\n");
                deleteMenuOptions.add("No\n");
                Menu confirmDeleteMenu = new Menu(deleteMenuTitle,deleteMenuOptions);

                int deleteOptionChosen = 0;
                while (deleteOptionChosen == 0)
                {
                    userInput.clearScreen();
                    confirmDeleteMenu.displayMenu();
                    deleteOptionChosen = userInput.readIntegerFromUser(2,
                            "Please select from options above (1-2): ");
                }

                if (deleteOptionChosen == 1)
                {
                    movieDatabase.deleteMovie(moviesForDelete.get(0));
                    System.out.print("\nSuccessfully deleted.\n\n");
                }
                else
                {
                    System.out.print("\nDeletion Cancelled.\n\n");
                }
            }
            catch (IllegalStateException e)
            {
                System.out.print("\nError creating menu!\n");
            }
        }
    }

}
