import java.util.ArrayList;
import java.util.Iterator;

public class Menu
{
    private String menuTitle;
    private ArrayList<String> menuOptions;

    public Menu(String title, ArrayList<String> options)
    {
        if ((title.trim().length() == 0) || (options.size() < 2))
        {
            throw new IllegalStateException("Must provide non-empty title and at least 2 options");
        }

        menuTitle = title;
        menuOptions = options;
    }

    public void displayMenu()
    {
        System.out.print(menuTitle);
        System.out.print("====================\n");
        for (int i = 0; i < menuOptions.size(); i++)
        {
            System.out.print("(" + (i+1) + "): " + menuOptions.get(i));
        }
    }
}
