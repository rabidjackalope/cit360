import java.util.ArrayList;
import java.util.Iterator;
import java.io.*;
import java.util.Scanner;

public class Database
{
    private ArrayList<Movie> movies;

    public Database()
    {
        movies = new ArrayList<Movie>();
    }

    public Database(ArrayList<Movie> newMovies)
    {
        movies = newMovies;
    }

    public void loadDatabaseFromFile(String filename)
    {
        try
        {
            FileReader fr = new FileReader(filename);

            try
            {
                Scanner scan = new Scanner(fr);
                int lineNumber = 0;
                while (scan.hasNextLine())
                {
                    lineNumber++;
                    String line = scan.nextLine();
                    String[] parts = line.split(",");
                    String title = parts[0];
                    String director = parts[1];
                    String mpaaRating = parts[2];
                    String format = parts[3];

                    try
                    {
                        Movie movie =  new Movie(title,director,mpaaRating,format);
                        addMovie(movie);
                    }
                    catch (IllegalStateException e)
                    {
                        System.out.print("Error in movie information read from file.\n");
                    }
                }
            }
            finally
            {
                fr.close();
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.print("File not found\n");
        }
        catch (IOException e)
        {
            System.out.print("Unexpected I/O exception\n");
        }
    }

    public void addMovie(Movie newMovie)
    {
        if (newMovie == null)
        {
            throw new NullPointerException("Must provide a valid movie object\n");
        }

        movies.add(newMovie);
    }

    public void clearAll()
    {
        movies.clear();
    }

    public void deleteMovie(Movie movieToDelete)
    {
        movies.remove(movieToDelete);
    }

    public void saveMoviesToFile(String filename)
    {
        try
        {
            PrintWriter writer = new PrintWriter(filename);
            try
            {
                Iterator<Movie> it = movies.iterator();
                while (it.hasNext())
                {
                    Movie movie = it.next();
                    String title = movie.getTitle();
                    String director = movie.getDirector();
                    String mpaaRating = movie.getMPAARating();
                    String format = movie.getFormat();
                    writer.write(title+","+director+","+mpaaRating+","+format+"\n");
                }
            }
            finally
            {
                writer.close();
            }
        }
        catch (IOException e)
        {
            System.out.print("Unexpected I/O exception\n");
        }
    }

    public ArrayList<Movie> searchForMovie(String searchString, String searchKey)
    {
        ArrayList<Movie> foundMovies = new ArrayList<Movie>();
        Iterator<Movie> it = movies.iterator();
        String searchStringLower = searchString.toLowerCase();

        while (it.hasNext())
        {
            Movie movie = it.next();
            String title = movie.getTitle().toLowerCase();
            String director = movie.getDirector().toLowerCase();
            String mpaaRating = movie.getMPAARating().toLowerCase();

            if (searchKey.equals("title"))
            {
                if (title.equals(searchStringLower))
                {
                    foundMovies.add(movie);
                }
            } else if (searchKey.equals("director")) {
                if (director.equals(searchStringLower))
                {
                    foundMovies.add(movie);
                }
            } else if (searchKey.equals("mpaaRating")) {
                if (mpaaRating.equals(searchStringLower)) {
                    foundMovies.add(movie);
                }
            }    else {
                System.out.print("\nError: Search key does not exist.\n");
            }
        }
        return foundMovies;
    }

}