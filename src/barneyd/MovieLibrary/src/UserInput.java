import java.util.Scanner;

public class UserInput
{
    final static String ESC = "\033[";

    public UserInput() {

    }

    public void clearScreen()
    {
        System.out.print(ESC + "2J");
    }

    private boolean isStringNumeric(String stringToCheck)
    {
        for (int i = 0; i < stringToCheck.length(); i++)
        {
            String c = stringToCheck.substring(i,i+1);
            if (!(c.equals("0") || c.equals("1") || c.equals("2") || c.equals("3") ||
                    c.equals("4") || c.equals("5") || c.equals("6") || c.equals("7") ||
                    c.equals("8") || c.equals("9")))
            {
                return false;
            }
        }
        return true;
    }

    public void pressEnterToContinue()
    {
        Scanner console = new Scanner(System.in);
        System.out.print("Press enter to continue...");
        console.nextLine();
    }

    public int readIntegerFromUser(int maxNumber, String inputPrompt)
    {
        Scanner console = new Scanner(System.in);
        boolean error = true;
        int numberChosen = 0;
        int inputNumber = -1;

        System.out.print("\n"+inputPrompt);
        String input = console.nextLine();
        input = input.trim();

        if (input.length() == 0)
        {
            System.out.print("\nError: Please Make a selection from the choices above.\n");
            pressEnterToContinue();
            error = true;
        }
        else
        {
            boolean inputIsNumber = isStringNumeric(input);
            if (inputIsNumber)
            {
                inputNumber = Integer.parseInt(input);

                if (maxNumber > 0)
                {
                    if ((inputNumber <= maxNumber) && (inputNumber > 0))
                    {
                        numberChosen = inputNumber;
                        error = false;
                    }
                    else
                    {
                        System.out.print("\nError: Please choose between 1 and " + maxNumber + ".\n");
                        pressEnterToContinue();
                        error = true;
                    }
                }
                else
                {
                    numberChosen = inputNumber;
                    error = false;
                }
            }
            else
            {
                System.out.print("\nError: Choice must be a number.\n");
                pressEnterToContinue();
                error = true;
            }
        }

        if (error)
        {
            return 0;
        }
        else
        {
            return numberChosen;
        }
    }

    public String readStringFromUser(String messagePrompt, boolean emptyStringAllowed)
    {
        Scanner console = new Scanner(System.in);
        System.out.print(messagePrompt);
        String inputName = console.nextLine();
        inputName = inputName.trim();

        if (!emptyStringAllowed)
        {
            while (inputName.length() == 0)
            {
                clearScreen();
                System.out.print("Error: Please enter at least one character.\n");
                System.out.print(messagePrompt);
                inputName = console.nextLine();
                inputName = inputName.trim();
            }
        }
        return inputName;
    }
}
