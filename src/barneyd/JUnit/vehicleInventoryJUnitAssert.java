package JUnit;
/*
 Week 5 Sources
 https://junit.org/junit5/docs/current/user-guide/index.html#writing-tests-assertions
 https://www.tutorialspoint.com/junit/junit_writing_tests.htm
 https://dzone.com/articles/junit-tutorial-beginners
*/

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import static org.junit.Assert.*;

public class vehicleInventoryJUnitAssert {

    public static void main(String[] args) {

    /*
     This method checks JUnit assertions and displays true if all assertions pass the test.
     Displays false if it does not pass the test and also displays which assertion failed the test and why.
    */
        Result result = JUnitCore.runClasses(vehicleInventoryJUnitAssert.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());

    }

    @Test
    public void testVehicle() {
        // This test fails on purpose for testing the above method.
        String vehicle1 = new String ("1967 Chevrolet Impala");
        String vehicle2 = new String ("1968 Chevrolet Impala");
        assertEquals(vehicle1, vehicle2);
    }
    @Test
    public void getcolor() {
        String color1 = "Black";
        String color2 = "Black";
        assertSame(color1, color2);
    }
    @Test
    public void testLength() {
        int length1 = 18;
        int length2 = 17;
        assertNotSame(length1, length2);
    }
    @Test
    public void testTVShowField() {
        String TVShow1 = "Supernatural";
        String TVShow2 = null;
        //Check if TVShow1 is not null.
        assertNotNull(TVShow1);
        //Check if TVShow2 is null.
        assertNull(TVShow2);
    }
    @Test
    public void testEquipment() {
        String[] equipment1 = {"4 door", "502 big-block", "Hotchkiss Suspension", "Hardtop"};
        String[] equipment2 = {"4 door", "502 big-block", "Hotchkiss Suspension", "Hardtop"};
        //Check if the two cars have the same list and order of equipment.
        assertArrayEquals(equipment1, equipment2);
    }
    @Test
    public void checkInventory() {
        int inv1 = 1;
        int inv2 = 2;
        assertTrue(inv1 < inv2);
        assertFalse(inv2 < inv1);
    }

}
