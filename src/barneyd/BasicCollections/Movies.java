package barneyd.BasicCollections;

/*
 * Modified from BYU-Idaho CIT 360 instructional material by Troy Tuckett
 */

public class Movies {

    private String title;
    private String rating;
    private String format;

    public Movies(String title, String rating, String format) {
        this.title = title;
        this.rating = rating;
        this.format = format;
    }

    public String toString() {
        return "Title: " + title + "," + " Rating: " + rating + "," + " Format: " + format;
    }
}
