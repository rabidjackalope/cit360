package barneyd.BasicCollections;

/*
 * Modified from BYU-Idaho CIT 360 instructional material by Troy Tuckett
 */

import java.util.*;

public class CollectionTypes {

    public static void main(String[] args) {

        System.out.println("-------------");
        System.out.println("Array List --");
        System.out.println("-------------");
        List familyList = new ArrayList();
        familyList.add("David");
        familyList.add("Carol");
        familyList.add("Jacob");
        familyList.add("William");
        familyList.add("Kaylee");
        familyList.add("Emma");
        familyList.add("Caitlin");

        System.out.println();
        System.out.println("--Unsorted--");
        for (Object str : familyList) {
            System.out.println((String)str);
        }

        System.out.println();
        System.out.println("--Sorted--");
        Collections.sort(familyList);
        for (int i=0; i<familyList.size(); i++)
            System.out.println(familyList.get(i));

        System.out.println();
        System.out.println("------");
        System.out.println("Set --"); //Cannot have duplicate records. Automatically sorted in natural order.
        Set familySet = new TreeSet();
        System.out.println("------");
        familySet.add("David");
        familySet.add("Carol");
        familySet.add("Jacob");
        familySet.add("William");
        familySet.add("David"); //This duplicated item will not display in output.
        familySet.add("Kaylee");
        familySet.add("Emma");
        familySet.add("Caitlin");

        for (Object str : familySet) {
            System.out.println((String)str);
        }

        System.out.println();
        System.out.println("--------");
        System.out.println("Queue --");
        System.out.println("--------");
        Queue familyQueue = new PriorityQueue(); //Automatically sorted in natural order.
        familyQueue.add("David");
        familyQueue.add("Carol");
        familyQueue.add("Jacob");
        familyQueue.add("William");
        familyQueue.add("Kaylee");
        familyQueue.add("Emma");
        familyQueue.add("Caitlin");

        Iterator iterator = familyQueue.iterator();
        while (iterator.hasNext()) {
            System.out.println(familyQueue.poll());
        }

        System.out.println();
        System.out.println("------");
        System.out.println("Map --");
        System.out.println("------");
        //Utilizes key, value pairs. Each key must be unique.
        Map familyMap = new HashMap();
        familyMap.put(1,"David");
        familyMap.put(2,"Carol");
        familyMap.put(3,"Jacob");
        familyMap.put(4,"William");
        familyMap.put(5,"Kaylee");
        familyMap.put(6,"Emma");
        //When a key is repeated, the previous value is replaced before output.
        familyMap.put(2,"Caitlin"); //This statement replaced "Carol" with "Caitlin" as shown in output.

        for (int i = 1; i < 7; i++) {
            String result = (String)familyMap.get(i);
            System.out.println(result);
        }

        System.out.println();
        System.out.println("----------------------");
        System.out.println("List using Generics --");
        System.out.println("----------------------");
        System.out.println("This is a list of movies in my collection");
        List<Movies> myMovies = new LinkedList<Movies>();
        myMovies.add(new Movies("Raiders of the Lost Ark", "PG", "LaserDisc"));
        myMovies.add(new Movies("Jurassic World", "PG-13", "Blu-Ray"));
        myMovies.add(new Movies("The Matrix", "R", "DVD"));
        myMovies.add(new Movies("Shrek 2", "PG", "DVD"));
        myMovies.add(new Movies("Cars", "G", "DVD"));
        myMovies.add(new Movies("War of the Worlds", "PG-13", "Blu-Ray"));

        for (Movies movies : myMovies) {
            System.out.println(movies);
        }

    }
}