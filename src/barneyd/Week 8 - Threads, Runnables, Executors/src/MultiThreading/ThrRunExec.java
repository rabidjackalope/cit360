package MultiThreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThrRunExec {

    public static void main(String args[]) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        carAssembly R1 = new carAssembly("Ford Mustang", 1000);
        carAssembly R2 = new carAssembly("Chevrolet Impala", 100);
        carAssembly R3 = new carAssembly("Lamborghini Aventador",10000);
        carAssembly R4 = new carAssembly("Honda CTX700",25);

        myService.execute(R1);
        myService.execute(R2);
        myService.execute(R3);
        myService.execute(R4);

        myService.shutdown();

    }

}