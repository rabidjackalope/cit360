package MultiThreading;

public class carAssembly implements Runnable {
    private String car;
    private int sleep;

    carAssembly(String car, int sleep) {
        this.car = car;
        this.sleep = sleep;

        System.out.println(car + " order in.");
        System.out.println("Ordering parts for " + car + ".\n" );

    }

    public void run() {
        System.out.println("Starting " + car + " assembly.\n");
        try {
            for(int i = 1; i > 0; i--) {
                System.out.println("Assembling " + car + ".\n" );
                Thread.sleep(sleep);
            }
        }catch (InterruptedException e) {
            System.out.println("Error assembling" + car + ".\n");
        }
        System.out.println(car + " is assembled and ready!\n");
    }

}