package barneyd.ErrorChecking;
import java.util.*;

public class dividesZero {

        /*In mathematics, you are allowed to divide by a negative number but not a zero.
        This program will only check if the divisor is 0. It will return an error message if a 0 is entered and
        allow the user to reenter the second number and then return the result.
        */
        public static void main(String[] args) {
            Scanner num = new Scanner(System.in);
            float number1;
            float number2;
            float result;

            System.out.println();
            System.out.println("This program divides two numbers and returns the result.");

            System.out.println();
            System.out.println("Enter your first number ");
            number1 = num.nextInt();

            do {
                System.out.println();
                System.out.println("Enter your second number ");
                number2 = num.nextInt();
                if (number2 == 0) {
                    System.out.println();
                    System.out.println("Please do not attempt to divide by zero.");
                    System.out.println("You may rip a hole in the space-time continuum. Try again.");
                }
            } while (number2 == 0);

            result = division(number1, number2);
            System.out.println();
            System.out.println(number1 + "/" + number2 + " = " + result);
            num.close();

        }

        private static float division (float number1, float number2) throws ArithmeticException {
            return (number1 / number2);
        }
    }



